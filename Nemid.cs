﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public static class NemIdHelper
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="plainText"></param>
    /// <returns></returns>
    public static string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public static string CprSignonXmlDocument(string userId, string password)
    {
        string cprXmlString = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>";
        cprXmlString += "<root xmlns=\"http://www.cpr.dk\">";
        cprXmlString += "<Gctp v=\"1.0\">";
        cprXmlString += "<Sik function=\"signon\" userid=\"" + userId + "\" password=\"" + password + "\" />";
        cprXmlString += "</Gctp>";
        cprXmlString += "</root>";

        return cprXmlString;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="cpr"></param>
    /// <returns></returns>
    public static string CprNameXmlDocument(string cpr)
    {
        string nameXmlString = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>";
        nameXmlString += "<root xmlns=\"http://www.cpr.dk\">";
        nameXmlString += "<Gctp v=\"1.0\">";
        nameXmlString += "<System r=\"CprSoeg\">";
        nameXmlString += "<Service r=\"NAVNE3\">";
        nameXmlString += "<CprServiceHeader r=\"NAVNE3\">";
        nameXmlString += "<Key>";
        nameXmlString += "<Field r=\"PNR\" v=\"" + cpr + "\" />";
        nameXmlString += "</Key>";
        nameXmlString += "</CprServiceHeader>";
        nameXmlString += "</Service>";
        nameXmlString += "</System>";
        nameXmlString += "</Gctp>";
        nameXmlString += "</root>";

        return nameXmlString;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="cpr"></param>
    /// <returns></returns>
    public static string CprAddressXmlDocument(string cpr)
    {
        string adrXmlString = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>";
        adrXmlString += "<root xmlns=\"http://www.cpr.dk\">";
        adrXmlString += "<Gctp v=\"1.0\">";
        adrXmlString += "<System r=\"CprSoeg\">";
        adrXmlString += "<Service r=\"ADRESSE3\">";
        adrXmlString += "<CprServiceHeader r=\"ADRESSE3\">";
        adrXmlString += "<Key>";
        adrXmlString += "<Field r=\"PNR\" v=\"" + cpr + "\" />";
        adrXmlString += "</Key>";
        adrXmlString += "</CprServiceHeader>";
        adrXmlString += "</Service>";
        adrXmlString += "</System>";
        adrXmlString += "</Gctp>";
        adrXmlString += "</root>";

        return adrXmlString;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="cpr"></param>
    /// <returns></returns>
    public static uint AgeFromCpr(string cpr)
    {
        if (string.IsNullOrEmpty(cpr) || cpr.Length < 6)
        {
            return 0;
        }

        var controlNumber = int.Parse(cpr.Substring(6, 1));
        var year = int.Parse(cpr.Substring(4, 2));
        var century = "";

        switch (controlNumber)
        {
            case 0:
            case 1:
            case 2:
            case 3:
                century = "19";
                break;
            case 4:
            case 9:
                if (year <= 36)
                {
                    century = "20";
                }
                else
                {
                    century = "19";
                }
                break;
            case 5:
            case 6:
            case 7:
            case 8:
                if (year <= 36)
                {
                    century = "20";
                }
                else if (year >= 58)
                {
                    century = "18";
                }
                break;
        }
        var birthday = new DateTime(int.Parse(century + year.ToString().PadLeft(2, '0')), int.Parse(cpr.Substring(2, 2)), int.Parse(cpr.Substring(0, 2)));
        return (uint)((new DateTime(1, 1, 1) + (DateTime.Today - birthday)).Year - 1);
    }
}


public class NemIdParam
{
    public string CLIENTFLOW { get; set; }
    //public string LANGUAGE { get; set; }
    //public string ORIGIN { get; set; }
    //public string REMEMBER_USERID_INITIAL_STATUS { get; set; }
    public string SP_CERT { get; set; }
    public string TIMESTAMP { get; set; }
    public string DIGEST_SIGNATURE { get; set; }
    public string PARAMS_DIGEST { get; set; }
    public string _certificatePath { get; set; }
    public string _certificatePassword { get; set; }

    public NemIdParam(string rememberUser, string origin, string timestamp, string language, string clientFlow, string certificatePath, string certificatePassword)
    {
        CLIENTFLOW = clientFlow;
        //LANGUAGE = language;
        //ORIGIN = origin;
        //REMEMBER_USERID_INITIAL_STATUS = rememberUser;
        _certificatePath = certificatePath;
        _certificatePassword = certificatePassword;

        SP_CERT = GetRawCert();
        TIMESTAMP = timestamp;
    }

    private Dictionary<string, string> GetParameters()
    {
        var _parameters = new Dictionary<string, string>();
        _parameters.Add("CLIENTFLOW", CLIENTFLOW);
        _parameters.Add("SP_CERT", SP_CERT);
        _parameters.Add("TIMESTAMP", TIMESTAMP);
        return _parameters;
    }

    private SortedDictionary<string, string> GetSortedParameters()
    {
        var _parameters = GetParameters();
        return new SortedDictionary<string, string>(_parameters, StringComparer.CurrentCultureIgnoreCase);
    }

    private byte[] GetNormalizedParameters()
    {
        var sb = new System.Text.StringBuilder();
        var sortedParameters = GetSortedParameters();

        foreach (var entry in sortedParameters)
        {
            sb.Append(entry.Key + entry.Value);
        }

        return System.Text.Encoding.UTF8.GetBytes(sb.ToString());
    }

    private byte[] CalculateDigest(byte[] data)
    {
        var sha = System.Security.Cryptography.SHA256.Create();
        byte[] digest = sha.ComputeHash(data);
        return digest;
    }

    private byte[] CalculateSignature(byte[] data)
    {
        try
        {
            var csp = (System.Security.Cryptography.RSACryptoServiceProvider)GetCertificate().PrivateKey;
            return csp.SignData(data, System.Security.Cryptography.CryptoConfig.MapNameToOID("SHA256"));
        }
        catch (System.Security.Cryptography.CryptographicException ce)
        {
            var cert = new System.Security.Cryptography.X509Certificates.X509Certificate2(_certificatePath, _certificatePassword, System.Security.Cryptography.X509Certificates.X509KeyStorageFlags.Exportable);

            var rsa = cert.PrivateKey as System.Security.Cryptography.RSACryptoServiceProvider;
            byte[] privateKeyBlob = rsa.ExportCspBlob(true);
            var rsa2 = new System.Security.Cryptography.RSACryptoServiceProvider();
            rsa2.ImportCspBlob(privateKeyBlob);
            return rsa2.SignData(data, "SHA256");
        }
    }

    private string GetRawCert()
    {
        var uidCert = GetCertificate();

        var base64Encoded = Convert.ToBase64String(uidCert.Export(System.Security.Cryptography.X509Certificates.X509ContentType.Cert));
        return base64Encoded.Replace("\r", "").Replace("\n", "");
    }

    private System.Security.Cryptography.X509Certificates.X509Certificate2 GetCertificate()
    {
        return new System.Security.Cryptography.X509Certificates.X509Certificate2(_certificatePath, _certificatePassword);
    }

    public string getJson()
    {
        byte[] normalizedParameters = GetNormalizedParameters();
        byte[] parameterDigest = CalculateDigest(normalizedParameters);
        byte[] parameterSignature = CalculateSignature(normalizedParameters);

        var digestString = Convert.ToBase64String(parameterDigest);
        var signatureString = Convert.ToBase64String(parameterSignature);

        var _parameters = GetParameters();
        _parameters.Add("PARAMS_DIGEST", digestString);
        _parameters.Add("DIGEST_SIGNATURE", signatureString);
        //return System.Web.Helpers.Json.Encode(_parameters);
        return JsonConvert.SerializeObject(_parameters);
    }
}

public class NemIdLogon
{
    static readonly List<string> ErrorCodes = new List<string> { "APP001","APP002","APP003","APP004","APP005","SRV001","SRV002","SRV003","SRV004","SRV005","SRV006","CAN001","CAN002",
"AUTH001","AUTH003","AUTH004","AUTH005","AUTH006","AUTH007","AUTH008","AUTH009","AUTH012","LOCK001","LOCK002","LOCK003","OCES001","OCES002","OCES003","OCES004","OCES005","OCES006"};

    private string _signature;
    public bool isEnvironmentSet
    {
        get
        {
            if (HttpContext.Current.Session["isEnvironmentSet"] == null)
            {
                return false;
            }
            return HttpContext.Current.Session["isEnvironmentSet"].ToString() == "true";
        }
        set
        {
            HttpContext.Current.Session["isEnvironmentSet"] = value.ToString();
        }
    }
    public NemIdLogon(string signature)
    {
        //SetEnvironment("OcesII_DanidEnvPreprod");
        SetEnvironment("OcesII_DanidEnvProd");
        _signature = Base64Decode(signature);
    }

    private string Base64Decode(string s)
    {
        var bytes = Convert.FromBase64String(s);
        return System.Text.Encoding.UTF8.GetString(bytes);
    }

    private string GetError()
    {
        return ErrorCodes.Contains(_signature.ToUpper()) ? _signature.ToUpper() : null;
    }

    private org.openoces.ooapi.signatures.OpenlogonSignature CreateOpenlogonSignature()
    {
        var error = GetError();
        if (error != null)
        {
            throw new Exception("An error has occured: " + error);
        }
        var abstractSignature = org.openoces.ooapi.signatures.OpensignSignatureFactory.Instance.GenerateOpensignSignature(_signature);
        if (!(abstractSignature is org.openoces.ooapi.signatures.OpenlogonSignature))
        {
            throw new ArgumentException("argument of type " + abstractSignature.GetType() +
                                        " is not valid output from the logon applet");
        }
        var signature = (org.openoces.ooapi.signatures.OpenlogonSignature)abstractSignature;
        return signature;
    }

    private void SetEnvironment(string environment)
    {
        if (isEnvironmentSet)
        {
            return;
        }

        try
        {
            var env = string.IsNullOrEmpty(environment) ? org.openoces.ooapi.environment.Environments.TrustedEnvironments.FirstOrDefault() : (org.openoces.ooapi.environment.OcesEnvironment)Enum.Parse(typeof(org.openoces.ooapi.environment.OcesEnvironment), environment, true);
            org.openoces.ooapi.environment.Environments.OcesEnvironments = new List<org.openoces.ooapi.environment.OcesEnvironment> { env };
        }
        catch { }

        isEnvironmentSet = true;
    }

    public org.openoces.serviceprovider.CertificateAndStatus ExtractCertificateAndStatus()
    {
        var signature = CreateOpenlogonSignature();
        if (signature.Verify())
        {
            var certificate = signature.SigningCertificate;
            var status = certificate.ValidityStatus();

            if (status == org.openoces.ooapi.certificate.CertificateStatus.Valid && org.openoces.serviceprovider.ServiceProviderSetup.CurrentChecker.IsRevoked(certificate))
            {
                status = org.openoces.ooapi.certificate.CertificateStatus.Revoked;
            }
            return new org.openoces.serviceprovider.CertificateAndStatus(certificate, status);
        }
        throw new Exception("The signature of login data is invalid. Data is: " + _signature);
    }
}

